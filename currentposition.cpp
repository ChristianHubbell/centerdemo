#include <QTimer>
#include "currentposition.h"

CurrentPosition::CurrentPosition(QObject *parent) : QObject(parent)
{
    setPosition(QGeoCoordinate(42.375953, -83.395381));

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updatePosition()));
    timer->start(100);
}

void CurrentPosition::setPosition(const QGeoCoordinate &c)
{
    if (currentPosition == c)
        return;

    currentPosition = c;
    emit positionChanged();

    emit centerMap();
}

QGeoCoordinate CurrentPosition::position() const
{
    return currentPosition;
}

void CurrentPosition::updatePosition()
{
    QGeoCoordinate pos;
    pos.setLatitude(currentPosition.latitude() + .0001);
    pos.setLongitude(currentPosition.longitude() + .0001);
    setPosition(pos);
}
