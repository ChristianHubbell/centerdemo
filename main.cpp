#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtWebEngine/qtwebengineglobal.h>
#include <QQmlContext>
#include "currentposition.h"

int main(int argc, char *argv[])
{

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QtWebEngine::initialize();
    CurrentPosition curpos;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("curPos", &curpos);

    engine.load(QUrl(QLatin1String("qrc:/pages/main/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
