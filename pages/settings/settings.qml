import QtQuick 2.6
import QtQuick.Controls 2.1

Page {
    id: settingsView

    background: Rectangle {
        anchors.fill: settingsView
        opacity: 1
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#771133" }
                GradientStop { position: 1.0; color: "#555555" }
            }
    }
}
