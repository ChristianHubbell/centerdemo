import QtQuick 2.8
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.1

ApplicationWindow {
    id: window
    visible: true
    width: 1280
    height: 720
    color: "#000000"
    title: qsTr("Rivian Center Stack")

    header: ToolBar {
        id: overlayHeader
        height: 20
        background: Rectangle {
            id: headerRec
            anchors.fill: parent
            color: "#000000"
        }

        Image {
            id: imageLink
            x: 4
            y: 3
            fillMode: Image.PreserveAspectFit
            source: "img/link.png"
        }

        Label {
            id: time
            x: (parent.width - time.width - 4)
            y: 3
            text: qsTr("12:08PM")
        }
    }

    footer: ToolBar {
        id: overlayFooter
        height: 60
        background: Rectangle {
            id: footerRec
            anchors.fill: parent
            color: "#000000"
        }

        Image {
            id: imageHVACLeft
            x: 40
            y: 30 - 13
            fillMode: Image.PreserveAspectFit
            source: "img/hvac.png"
        }

        Image {
            id: imageHVACRight
            x: parent.width - imageHVACRight.width - 40
            y: 30 - 13
            fillMode: Image.PreserveAspectFit
            source: "img/hvac.png"
        }
    }

    Image {
        id: image
        x: (window.width /2) - (image.width/2)
        y: ((window.height - overlayHeader.height - overlayFooter.height)/2) - (image.height/2)
        width: 64
        height: 64
        fillMode: Image.PreserveAspectFit
        source: "img/background.png"
    }

    ListModel {
        id: sideListModel
        ListElement { title: "Home"; source: "qrc:/pages/home/home.qml"; icon: "qrc:/pages/main/img/home.png"; tag:"Home" }
        ListElement { title: "Music"; source: "qrc:/pages/music/music.qml"; icon: "qrc:/pages/main/img/music.png"; tag:"Music" }
        ListElement { title: "Navigation"; source: "qrc:/pages/navigation/navigation.qml"; icon: "qrc:/pages/main/img/navigation.png"; tag:"Navigation" }
        ListElement { title: "Navigation2"; source: "qrc:/pages/navigation/webnav/navigation-mapbox-html.qml"; icon: "qrc:/pages/main/img/navigation.png"; tag:"Navigation2" }
        ListElement { title: "Navigation3"; source: "qrc:/pages/navigation/navigation-Here.qml"; icon: "qrc:/pages/main/img/navigation.png"; tag:"Navigation3" }
        ListElement { title: "Navigation4"; source: "qrc:/pages/navigation/navigation-OSM.qml"; icon: "qrc:/pages/main/img/navigation.png"; tag:"OSM" }
        ListElement { title: "Car Play"; source: "qrc:/pages/carplay/carplay.qml"; icon: "qrc:/pages/main/img/carplay.png"; tag:"CarPlay" }
        ListElement { title: "Contacts"; source: "qrc:/pages/contacts/contacts.qml"; icon: "qrc:/pages/main/img/contacts.png"; tag:"Contacts" }
        ListElement { title: "Vehicle"; source: "qrc:/pages/vehicle/vehicle.qml"; icon: "qrc:/pages/main/img/vehicle.png"; tag:"Vehicle" }
        ListElement { title: "Camera"; source: "qrc:/pages/cameras/cameras.qml"; icon: "qrc:/pages/main/img/cameras.png"; tag:"Cameras" }
        ListElement { title: "Settings"; source: "qrc:/pages/settings/settings.qml"; icon: "qrc:/pages/main/img/settings.png"; tag:"Settings" }
        ListElement { title: "Autonomous"; source: "qrc:/pages/adas/autonomous.qml"; icon: "qrc:/pages/main/img/autonomous.png"; tag:"ADAS" }
    }

    Component {
        id: sideListDelegate

        Item {
            id: delegateItem
            width: 60
            height: 60

            Image {
                id: listIcon
                width: 54
                height: 54
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                source: icon
            }
            //Text {
            //    anchors { top: listIcon.bottom; horizontalCenter: parent.horizontalCenter }
            //    text: title
            //}
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    listView.currentIndex = index
                    stackView.push(source)
                    //drawer.close()
                }
            }
        }
    }

    Drawer {
        id: drawer
        y: overlayHeader.height
        width: 60
        height: window.height - y - overlayFooter.height
        //interactive : false
        position: 1
        visible: true
        //modal: true
        dim: false

        background: Rectangle {
            anchors.fill: parent
            //color: "#000000"
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#303030"}
                GradientStop { position: 1.0; color: "#505050"}
            }
        }

        ListView {
            id: listView
            anchors.fill: parent
            model: sideListModel
            delegate: sideListDelegate
        }
    }

    StackView {
        id: stackView
        width: window.width
        height: (window.height - overlayHeader.height - overlayFooter.height)
    }
}
