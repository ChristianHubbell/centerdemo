import QtQuick 2.6
import QtQuick.Controls 2.1

Page {
    id: musinMenuView

    background: Rectangle {
        anchors.fill: cameraView
        opacity: 1
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#000000" }
                GradientStop { position: 1.0; color: "#555555" }
        }
    }

    Button {
        id: text1
        x: 82
        y: 177
        text: qsTr("Napster")
        font.family: "Tahoma"
        font.pixelSize: 12
        onClicked: {

        }
    }

    Button {
        id: text2
        x: 82
        y: 274
        text: qsTr("USB")
        font.pixelSize: 12
        font.family: "Tahoma"
    }

    Button {
        id: text3
        x: 82
        y: 77
        text: qsTr("FM")
        font.pixelSize: 12
        font.family: "Tahoma"
    }

            Button {
            id: text4
            x: 239
            y: 177
            text: qsTr("AUX")
            font.pixelSize: 12
            font.family: "Tahoma"
        }

        Button {
            id: text5
            x: 404
            y: 177

            text: qsTr("Soundcloud")
            font.pixelSize: 12
            font.family: "Tahoma"
        }

        Button {
            id: text6
            x: 239
            y: 77
            text: qsTr("SiriusXM")
            font.pixelSize: 12
            font.family: "Tahoma"
        }

        Button {
            id: text7
            x: 404
            y: 77
            text: qsTr("Bluetooth")
            font.pixelSize: 12
            font.family: "Tahoma"
        }



}
