import QtQuick 2.6
import QtQuick.Controls 2.1
import QtWebEngine 1.3

//import Qt.labs.settings 1.0
//import QtQml 2.2
//import QtQuick 2.2
//import QtQuick.Controls 1.0
//import QtQuick.Controls.Private 1.0 as QQCPrivate
//import QtQuick.Controls.Styles 1.0
//import QtQuick.Dialogs 1.2
//import QtQuick.Layouts 1.0
//import QtQuick.Window 2.1
//import QtWebEngine 1.3


Page {
    id: musicView

    WebEngineView {
        anchors.fill: parent

        settings.autoLoadImages: true
        settings.javascriptEnabled: true
        settings.errorPageEnabled: true
        settings.pluginsEnabled: true
        settings.fullScreenSupportEnabled: true
        settings.autoLoadIconsForPage: true
        settings.touchIconsEnabled: true

        url:  "qrc:/pages/music/demo.html"
        //url: "https://helpx.adobe.com/flash-player.html"
    }
}
