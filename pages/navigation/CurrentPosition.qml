import QtQuick 2.0
import QtLocation 5.3

MapQuickItem {
    id: currentpos
    property int bearing: 45;

    anchorPoint.x: image.width/2
    anchorPoint.y: image.height/2

    sourceItem: Image {
        id: image
        rotation: bearing
        source: "qrc:pages/navigation/img/currentpos.png"
    }
}
