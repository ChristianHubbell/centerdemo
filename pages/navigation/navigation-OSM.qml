import QtQuick 2.6
import QtQuick.Controls 2.1
import QtLocation 5.6
import QtPositioning 5.5

Page {
    id: navigation

    Map {
        id: mapOfEurope
        anchors.centerIn: parent
        anchors.fill: parent
        zoomLevel: 15
        //bearing: 15
        //center: QtPositioning.coordinate(42.375953, -83.395381)
        center: curPos.position

        copyrightsVisible: false
        //MapType: "mapbox://styles/mapbox/navigation-preview-night-v2"

        plugin: Plugin {
            name: "osm"
            //PluginParameter { name: "mapboxgl.mapping.additional_style_urls"; value: "mapbox://styles/mapbox/navigation-preview-night-v2" }
            //PluginParameter { name: "mapbox.access_token"; value: "pk.eyJ1IjoiY2h1YmJlbGwiLCJhIjoiY2o2d2k3eTFxMWN2MzMzcGQxdG5oNWdsbiJ9.kzYMAZqlC8O6upE9Ob7ppw" }
            //PluginParameter { name: "mapboxgl.access_token"; value: "pk.eyJ1IjoiY2h1YmJlbGwiLCJhIjoiY2o2d2k3eTFxMWN2MzMzcGQxdG5oNWdsbiJ9.kzYMAZqlC8O6upE9Ob7ppw" }
            //PluginParameter { name: "mapbox.mapping.additional_map_ids"; value: "mapbox://styles/mapbox/navigation-preview-night-v2" }
        }

        Component.onCompleted: {
            //addMapItem(new mapboxgl.NavigationControl())
         //   for( var i_type in supportedMapTypes ) {
          //      if( supportedMapTypes[i_type].name.localeCompare( "mapbox://styles/mapbox/navigation-preview-night-v2" ) === 0 ) {
           //         activeMapType = supportedMapTypes[i_type]
            //        break;
             //   }
            //}
        }

        // Add zoom and rotation controls to the map.
        //map.addControl(new mapboxgl.NavigationControl());


        CurrentPosition{
            id: cppCurrentPosition
            coordinate: curPos.position //

            Component.onCompleted: {
            }

            function centerMap(){
                mapOfEurope.Center = curPos.position
            }
        }

        MouseArea{

        }
    }
}
