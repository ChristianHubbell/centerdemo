import QtQuick 2.6
import QtQuick.Controls 2.1
import QtWebEngine 1.0

Page {
    id: contactsView

    WebEngineView {
        anchors.fill: parent
        url:  "navigation-mapbox.html"
    }
}
