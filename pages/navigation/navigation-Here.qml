import QtQuick 2.6
import QtQuick.Controls 2.1
import QtLocation 5.6
import QtPositioning 5.5

Page {
    id: navView

    property variant location: QtPositioning.coordinate(42.375953, -83.395381)

    Plugin {
        id: mapPluginHere
        name: "here"
        PluginParameter { name: "here.app_id"; value: "PcNFmbx0OyhOJxPnAdF1" }
        PluginParameter { name: "here.token"; value: "bA090lmf01KXrYj3o9g9BQ" }
    }

    PositionSource {
        id: positionSource
        property variant lastSearchPosition: location
        active: true
        updateInterval: 120000 // 2 mins
        onPositionChanged:  {
            var currentPosition = positionSource.position.coordinate
            map.center = currentPosition
        }
    }

    Map {
        id: map
        anchors.fill: parent
        plugin: mapPluginHere
        center: location
        zoomLevel: 14
    }
}
