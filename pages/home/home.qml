import QtQuick 2.6
import QtQuick.Controls 2.1

Page {
    id: homeView

    background: Rectangle {
        anchors.fill: homeView
        opacity: 1
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#001166" }
                GradientStop { position: 1.0; color: "#555555" }
            }
    }
}
