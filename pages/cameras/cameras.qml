import QtQuick 2.6
import QtQuick.Controls 2.1

Page {
    id: cameraView

    background: Rectangle {
        anchors.fill: cameraView
        opacity: 1
        gradient: Gradient {
                GradientStop { position: 0.0; color: "#660055" }
                GradientStop { position: 1.0; color: "#555555" }
            }
    }
}
