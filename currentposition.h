#include <QGeoCoordinate>

#ifndef CURRENTPOSITION_H
#define CURRENTPOSITION_H

#include <QObject>

class CurrentPosition : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QGeoCoordinate position READ position WRITE setPosition NOTIFY positionChanged)

public:
    explicit CurrentPosition(QObject *parent = nullptr);

    void setPosition(const QGeoCoordinate &c);

    QGeoCoordinate position() const;

signals:
    void positionChanged();
    void centerMap();

public slots:
     void updatePosition();

protected:

private:
    QGeoCoordinate currentPosition;
};

#endif // CURRENTPOSITION_H
